#!/usr/bin/python
import httplib, json
import email_alert_config

class Model:
    def __init__(self):
		self.config = email_alert_config.config
        
    def dbapiRequest(self, method, reqURL, params, headers):
        apiConn = httplib.HTTPConnection(self.config['dbapi']['host']+":"+self.config['dbapi']['port'])
        if params and headers :
            apiConn.request(method, "/"+self.config['dbapi']['url']+"/"+reqURL, params, headers)
        else :
            apiConn.request(method, "/"+self.config['dbapi']['url']+"/"+reqURL)

        return apiConn.getresponse()
        
    def emailapiRequest(self, method, reqURL, params, headers):
        apiConn = httplib.HTTPConnection(self.config['emailapi']['host']+":"+self.config['emailapi']['port'])
        if params and headers :
            apiConn.request(method, reqURL, params, headers)
        else :
            apiConn.request(method, reqURL)

        return apiConn.getresponse()
    
    def getJobs(self):
        print("[ Getting Jobs From DB API ] :"),
        listJobs = {}
        outResponse = self.dbapiRequest("GET", "process/alert/getTask", "", "")
        print(outResponse.reason)
        if outResponse.reason == 'OK':
            responseData = outResponse.read()
            listJobs = json.loads(responseData)
            if listJobs:
                print("  > [ Total Jobs ] : "+str(len(listJobs)))
            else :
                print("  > [ Total Jobs ] : 0")

        return listJobs
    
    def updateTaskStatus(self,taskID,taskStatus):
        print("[ Updating Task Status To : "+taskStatus+" ] :"),
        outResponse = self.dbapiRequest("PUT", "process/alert/updateStatus/"+taskID+"/"+taskStatus, "", "")
        print(outResponse.reason)

model = Model()