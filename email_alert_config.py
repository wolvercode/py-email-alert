#!/usr/bin/python
import json

class Config:
	def __init__(self):
		with open('email_alert.json', 'r') as f:
			configValue = json.load(f)
			self.currentConfig = configValue[configValue['active']]

config = Config().currentConfig