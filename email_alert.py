#!/usr/bin/python
import time, json
import email_alert_config
import email_alert_model

class Apps:
	def __init__(self):
		self.listJobs = {}
		self.config = email_alert_config.config
		self.model = email_alert_model.model

	def sendEmailMessage(self,jobs):
		print("  > [ Sending Email Alert ]")
		print("    > [ Receiver ] : "+jobs['receiver_email'])
		print("    > [ Mail Title ] : '"+jobs['mailtask_title']+"'")
		print("    > [ Sending Status ] :"),
		requestData = json.dumps({
			"email_receiver" : jobs['receiver_email'].split(","), 
			"email_title" : jobs['mailtask_title'], 
			"email_subject" : jobs['mailtask_subject'],
			"email_message" : jobs['mailtask_message'],
			"email_attachment" : jobs['mailtask_attachment'].split(",")
		})
		headers = { "Content-type": "application/json"}
		outResponse = self.model.emailapiRequest("POST", "/sendMail", requestData, headers)
		# print(outResponse.reason)
		resBody = json.loads(outResponse.read())
		print(resBody['Status']+", "+resBody['Message'])
		
		if resBody['Status'] == 'Success' :
			return jobs['mailtask_id'], "7"
		else:
			return jobs['mailtask_id'], "5"

	def doJobs(self):
		print("[ Do Jobs ]")
		for jobs in self.listJobs:
			taskID, taskStatus = self.sendEmailMessage(jobs)
			self.model.updateTaskStatus(str(taskID), taskStatus)
	
	def sleep(self):
		print("....Sleep for "+str(self.config['main']['sleep'])+" seconds....")
		time.sleep(self.config['main']['sleep'])

	def run(self):
		if len(self.config) == 0 :
			print ("[ Error ] : Config File is empty!")
			return

		print("....Starting "+self.config['main']['displayname']+"....")

		self.listJobs = self.model.getJobs()
		if self.listJobs is None or len(self.listJobs) == 0:
			return
		
		self.doJobs()

if __name__ == "__main__":
	while True:
		apps = Apps()
		apps.run()
		apps.sleep()
